/************************
* Devoir IALC 2018-19 - M1 Miage App 
* Binome : Nom1 Nom 2 	(TODO)
*
* Modèle : visites1.mod
* Description : TODO
*
************************/
using CP;

include "structures.mod";

Instance instance = ...;		/* instance de problème à résoudre */

/************************************************************************
* Lecture du fichier d'instance
************************************************************************/

include "structuresLecture.mod";

execute{
	includeScript("js/lectureInstance.js"); //Inclure script
	writeln(Opl.first(instance.files));
	recupInfo(Opl.first(instance.files));
}

main{
	writeln("ON COMMENCE----------------------------------------------------------_______________");
	//includeScript("js/lectureInstance.js"); //Inclure script
	//recupInfo("../data/instances/enonce/enonce1.txt");
	//recupInfo(instance.files);
	//recupInfo("../data/instances/enonce/enonce2.txt");	

	var mainSrc = new IloOplModelSource("visites1.mod");
	var mainModel = new IloOplModelDefinition(mainSrc);
	
	var mainCp = new IloCP();
	mainCp.param.logVerbosity = "Quiet";
	//mainCp.param.searchType="DepthFirst";
	var mainOpl = new IloOplModel(mainModel, mainCp);
	
	var mainData3 = new IloOplDataElements();
	mainData3.instance = thisOplModel.instance;
	mainOpl.addDataSource(mainData3);
	
	var mainData = new IloOplDataSource("../data/dat/adresses_essonne_91-410_870_530.dat");
	mainOpl.addDataSource(mainData);

	var mainData1 = new IloOplDataSource("data.dat");
	mainOpl.addDataSource(mainData1);

	mainOpl.generate();

	if(mainCp.solve()){
		mainOpl.postProcess();
	}
	else {
		writeln("No")
	}
}
