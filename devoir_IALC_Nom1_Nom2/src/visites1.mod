/************************
* Devoir IALC 2018-19 - M1 Miage App 
* Binome : Bouayad Ambert 	(TODO)
*
* Modèle : visites1.mod
* Description : TODO
*
************************/
using CP;

include "structures.mod";

Instance instance = ...;		/* instance de problème à résoudre */

/************************************************************************
* Lecture du fichier d'instance
************************************************************************/
include "structuresLecture.mod";
include "structuresResultat.mod";

tuple Coord {
	float longi;
	float lati;}

{CadastreAddress} addressBase = ...;

{addresse} addressPatient = ...;
{journee} journeeDixVin = ...;
{acte} actes = ...;
{soins} soinsInf = ...;
{indispo} indispos =...;
{infirmiere} infi =...;
{ecart} ecartSoins=...;



string modelName = "visites1";


{string} patients = {e.patient |e in addressPatient};
{string} addresses = {e.place |e in addressPatient};

float longi[patients];
float lati[patients];

int durations[patients][patients];
execute {  
	for(p in addressPatient){
		for(a in addressBase){
			if(p.place == a.place) {longi[p.patient] = a.longitude; lati[p.patient]=a.latitude;}
		}
	}

	includeScript("../libs/simpleJSONParser.js");
	includeScript("../libs/files.js");
	includeScript("../libs/geoServices.js");

	var coordOsrm = "";
	for(p in patients){
		if(coordOsrm != "") {coordOsrm = coordOsrm+";";}
		coordOsrm = coordOsrm + longi[p] + ","+lati[p];
	}
	
	var answer = osrm_table(coordOsrm, "osrm.dep-informatique.u-psud.fr:5000");

	var durations_temp = answer["durations"];

	for(p in patients){
		for(pp in patients){
			durations[p][pp] = Opl.ftoi(Opl.round(durations_temp[Opl.ord(patients,p)][Opl.ord(patients,pp)]/60.0));
		}
	}
}
	
/************************************************************************
* Prétraitement sur les données de l'instance 
************************************************************************/

/* TODO 
Déclaration des structures de données utiles pour faciliter
l'expression du modèle
*/


/************************************************************************
* Variables de décision
************************************************************************/

{string} mescodeacte = {a.codeActe| a in soinsInf};
int dureeSoin[mescodeacte] = [e.codeActe : e.dureeActe|e in actes];



int dureeSoinMin = min(i in mescodeacte) dureeSoin[i];
int dureeSoinMax = max(i in mescodeacte) dureeSoin[i];


int debutjournee = max(e in journeeDixVin) e.debut;
int finjournee = max(e in journeeDixVin) e.fin;


dvar interval IntSoins[soinsInf] in debutjournee .. finjournee size dureeSoinMin..dureeSoinMax;

dvar interval voyage[soinsInf] in debutjournee..finjournee size 0..120;

dvar interval attentes[soinsInf] in debutjournee..finjournee size 0..9999;


dvar interval travailTot[0..2][soinsInf] = [IntSoins,voyage,attentes];
dvar interval cover;

cumulFunction f = sum(i in 0..2, j in soinsInf) pulse(travailTot[i][j],1);

/************************************************************************
* Contraintes du modèle 					(NB : ne peut être mutualisé)
************************************************************************/

minimize (max (i in soinsInf, j in 0..2) endOf(travailTot[j][i])) - (min (i in soinsInf, j in 0..2) startOf(travailTot[j][i]));

subject to {

	noOverlap(all(i in soinsInf,j in 0..2) travailTot[j][i]);
	
	span(cover, travailTot);
	
	alwaysIn(f,cover,1,1);
	
	//chaque soins on verifie sa duree, le debut de l'acte et la fin de l'acte
	//La fin d'un soin doit etre le debut d'un voyage
	//la fin d'un voyage doit etre le début d'une attente
	//la fin d'une attente doit etre le debut d'un soin
	forall (s in soinsInf){
		sizeOf(IntSoins[s]) == dureeSoin[s.codeActe];
		startOf(IntSoins[s]) >= s.debutActe;
		endOf(IntSoins[s]) <= s.finActe; 


		endBeforeStart(travailTot[1][s],travailTot[0][s],sizeOf(travailTot[2][s]));
		startAtEnd(travailTot[2][s],travailTot[1][s],0);
		startAtEnd(travailTot[0][s],travailTot[2][s],0);
				
		forall(ss in soinsInf){
			(endOf(IntSoins[s]) == startOf(voyage[ss])) => (sizeOf(voyage[ss]) == durations[s.patient][ss.patient]);
		}

	}
	
	startOf(travailTot[0][<"cabinet","cab",0,1440>])>=debutjournee;
	
	endOf(travailTot[0][<"cabinet","cab",0,1440>])<=finjournee;
	
	endOf(travailTot[1][<"cabinet","cab",0,1440>])==startOf(travailTot[2][<"cabinet","cab",0,1440>]);
	endOf(travailTot[2][<"cabinet","cab",0,1440>])==startOf(travailTot[0][<"cabinet","cab",0,1440>]);

	
	min(i in soinsInf) startOf(travailTot[0][i]) >=  startOf(travailTot[0][<"cabinet","cab",0,1440>]);
	min(i in soinsInf) startOf(travailTot[1][i]) >=  startOf(travailTot[1][<"cabinet","cab",0,1440>]);
	min(i in soinsInf) startOf(travailTot[2][i]) >=  startOf(travailTot[2][<"cabinet","cab",0,1440>]);


	max(i in soinsInf) endOf(travailTot[0][i]) <=  endOf(travailTot[0][<"cabinet","cab2",0,1440>]);
	max(i in soinsInf) endOf(travailTot[1][i]) <=  endOf(travailTot[1][<"cabinet","cab2",0,1440>]);
	max(i in soinsInf) endOf(travailTot[2][i]) <=  endOf(travailTot[2][<"cabinet","cab2",0,1440>]);
	
	endOf(travailTot[1][<"cabinet","cab2",0,1440>])==startOf(travailTot[2][<"cabinet","cab2",0,1440>]);
	endOf(travailTot[2][<"cabinet","cab2",0,1440>])==startOf(travailTot[0][<"cabinet","cab2",0,1440>]);
}


execute {
	for(var i in soinsInf){
		writeln(" ");
		write("Mon soin est "+ i);
		write(" voyage "+ voyage[i]);
		write(" attente " + attentes[i]);
		write(" soins "+ IntSoins[i]);
		writeln("   -------   ");
	}
	

	//inclure le script qui remplira steps
}	


//Script pour la mise en forme du résultat
{Etape} mesEtapes = {};

string infirm;
int	etape =0;		
				
string visite;			
string adresse; 	
float latitude; 	 
float longitude;	 
string description;	
int debut;      	
int duree;      	
int fin;      		
int transit;
soins etapeAvant= <"cabinet","cab",0,1440>;
soins etapeMtn = <"cabinet","cab",0,1440>;
soins etapeFinale = <"cabinet","cab2",0,1440>;
int optimum = (max (i in soinsInf, j in 0..2) endOf(travailTot[j][i])) - (min (i in soinsInf, j in 0..2) startOf(travailTot[j][i]));
int flag =0;

execute {
	
	
	etapeMtn = etapeAvant;
	while(etapeMtn != etapeFinale) {
		infirm = (Opl.first(infi)).nom;
		
		for (p in addressPatient) {
			if(p.patient == etapeMtn.patient){

				
				adresse = p.place +""+p.ville;
				latitude = lati[p.patient];
				longitude = longi[p.patient];

			}
			
			
			
			
		}
		visite = etapeMtn.patient;
		description = etapeMtn.codeActe;
		
		debut = Opl.startOf(IntSoins[etapeMtn]);
		fin = Opl.endOf(IntSoins[etapeMtn]);
		duree = Opl.sizeOf(IntSoins[etapeMtn]);

		for(p in soinsInf){
			
			
			if(Opl.startOf(voyage[p])==Opl.endOf(IntSoins[etapeMtn]) && flag==0){
				if(etapeMtn != p){
					transit =Opl.sizeOf(voyage[p]);
					var etapeMtn = p; 	
					
					flag++;	
					
				}
			}
		}
		
		mesEtapes.add(infirm,etape,visite,adresse,latitude,longitude,description,debut,duree,fin,transit);
		etape = etape+1;
		flag =0;
		
		

	}
	
	mesEtapes.add(infirm,etape,"cabinet",(Opl.first(mesEtapes)).adresse,(Opl.first(mesEtapes)).latitude,(Opl.first(mesEtapes)).longitude,(Opl.first(mesEtapes)).description,Opl.startOf(voyage[etapeFinale]),0,Opl.endOf(voyage[etapeFinale]),0);

	

	includeScript("../libs/result.js");
	save_result(optimum,mesEtapes,modelName);

	
	
}

/* TODO */


/************************************************************************
* Contrôle de flux  (si besoin)
************************************************************************/

/* TODO */



/************************************************************************
* PostTraitement
************************************************************************/
{Etape} steps = mesEtapes;
include "postProcessingMap.mod";
/* TODO */

