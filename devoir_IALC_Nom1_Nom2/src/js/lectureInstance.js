/************************
* Devoir IALC 2018-19 - M1 Miage App 
* Binome : Bouayad Ambert 	(TODO)
*
* Fonctions de script utiles pour la lecture des fichiers d'instance
************************/

// NB comme pour n'importe quel langage de programmation, pour faciliter la 
// lisibilité de votre code, n'hésitez pas à le décomposer en plusieurs 
// fonctions

/* TODO */
function file_to_string(file){
	var f= new IloOplInputFile(file);
	if(f.exists) {
		var s = "";
		while (!f.eof) {
			s = s + f.readline() + "\n";
		}
		f.close();
	}else {
		writeln("\nWarning : the file ", f," doesn't exist");
	}
	return s;
}

function write_in_file(adresse, journee, soins, acte, indispo, inf, ecart){
	fichier = "data.dat";
	var f = new IloOplOutputFile(fichier);
	f.writeln(adresse);
	f.writeln(journee);
	f.writeln(indispo);
	f.writeln(soins);
	f.writeln(acte);
	f.writeln(inf);
	f.writeln(ecart);
	f.close();

}
			
function recupInfo(fichier){

	
	var chaine = file_to_string(fichier);
	var lignes = chaine.split("\n");
	var grandeurLignes = lignes.length;
	var i = 0;

	//chaine pour créer les tuples
	var indisponibilite = "indispos = {";
	var soins = "soinsInf = {	< \"cabinet\", \"cab\",0,1440>, \n";
	var adresse = "addressPatient = {";
	var infirmiere = "infi = {";
	var journee = "journeeDixVin = {";
	var a = "actes = {	< \"cab\",0>,\n";
	var e = "ecartSoins = {";

	
	

	for(i = 0; i < grandeurLignes ; i++){
		var mots = lignes[i].split(' ');
		var grandeurMots = mots.length;
		if(mots[0] == "soins"){
			var newMot = new Array();
			var lengthM = 0;
			for(j=0; j<grandeurMots; j++){
				if(mots[j] == '' || mots[j] == '\t' || mots[j] == 'soins'){
					delete mots[j];
				}else {
					newMot[lengthM] = mots[j];
					lengthM++;
				}
			}
			grandeurMots = newMot.length;
			var motSansTab = new Array();
			var lengthS = 0;
			for(j=0; j<grandeurMots; j++){
				if(newMot[j] != null){
					var temp = newMot[j];
					var temp2 = temp.split('\t');
					if(temp2.length >1){
						for(k=0; k<temp2.length; k++){
							if(temp2[k] == ''||temp2[k]=='soins'){delete temp2[k];}
							else { motSansTab[lengthS] = temp2[k];
								lengthS++;
							}
						}
					}else {
						motSansTab[lengthS] = temp;
						lengthS++;
					}
				}
			}
			grandeurMots = motSansTab.length;
			
			//On va faire le tri dans les soins
			var tempSoins = new Array();
			var tempLength = 3;
			tempSoins[0] = motSansTab[0];


			for(j=1; j<grandeurMots; j++){
				if(motSansTab[j] != null && motSansTab[j].length > 6){
					var deuxPoint = motSansTab[j].split(':');
					var plus = deuxPoint[0].split('+'); //Si deux soins
					//tranche horaire
					var debActe = "";
					var finActe = "";
					var acte = deuxPoint[1].split('-');
					if(acte.length > 1){
						if(acte[0].length>2){
							var heuredeb = acte[0].split('h');
							for(z=0; z<heuredeb[0].length; z++){
								if(heuredeb[0].charAt(z) != '['){
									debActe = debActe+ acte[0].charAt(z);
								}
							}
							
							if(heuredeb[1] == '30'){ debActe = debActe+'.5';}
						}
						if(acte[1].length>2){
							var heurefin = acte[1].split('h');
							for(z=0; z<heurefin[0].length; z++){
								if(heurefin[0].charAt(z) != ']'){
									finActe = finActe + acte[1].charAt(z);
								}
							}
							if(heurefin[1] == '30]'){finActe = finActe + ',5';}
						}
					}else {
						if(deuxPoint[1] == "soir"){
							debActe = 19; finActe = 24;
						} else {
							debActe = 0; finActe = 11;
						}
					}
					//transformation en minute
					debActe = parseFloat(debActe);
					finActe = parseFloat(finActe);
					debActe = debActe*60;
					finActe = finActe*60;
					tempSoins[2] = debActe;
					tempSoins[3] = finActe;
					//Quand 2 soins dans une tranche
					if(plus.length > 1){
						//1er soins dans tableau
						tempSoins[1] = plus[0];
						//rempli chaine
						soins = soins + "\t< \""+motSansTab[0]+"\", \""+tempSoins[1]+"\","+debActe+","+finActe+">,\n";
						//2eme soins
						tempSoins[1] = plus[1];
						tempLength++;
						soins = soins + "\t< \""+motSansTab[0]+"\", \""+tempSoins[1]+"\","+debActe+","+finActe+">, \n";
					}else {
						tempSoins[1]=deuxPoint[0];
						soins = soins + "\t< \""+motSansTab[0]+"\", \""+deuxPoint[0]+"\","+debActe+","+finActe+">, \n";
					}
				}else {
					//quand soin avec ecart
					var mini = "";
					var maxi = "";
					var ecart;
					if(motSansTab[j] != null && motSansTab[j].length <5){
						//le nom de l'acte
						tempSoins[1] = motSansTab[j];
						soins = soins + "\t< \""+motSansTab[0]+"\", \""+tempSoins[1]+"\",0,1440>, \n";
						if(j+1 != grandeurMots){
							ecart = motSansTab[j+1].split('<<');
							for(m=0; m<ecart[0].length; m++){
								if(ecart[0].charAt(m) != 'h'){
									mini =  mini+ecart[0].charAt(m);
								}
							}
							for(m=0; m<ecart[1].length; m++){
								if(ecart[1].charAt(m) != 'h'){
									maxi = maxi+ecart[1].charAt(m);
								}
							}
							//conversion en minutes
							mini = mini*60;
							maxi = maxi*60; 
							e = e + "\t <\""+motSansTab[0]+"\", \""+tempSoins[1]+"\","+mini+","+maxi+", \""+motSansTab[j+2]+"\">, \n";
						}
					}
						
				}	

				
			}


		}else if(mots[0] == "indisponibilite"){
			//on met dans des tuples ou variable
				var newMot = new Array();
				var lengthM = 0;
				for(j = 0; j<grandeurMots; j++){
					if(mots[j] == '' || mots[j] == '\t' || mots[j] == 'indisponibilite'){
						delete mots[j]; 
					}else {
						newMot[lengthM] = mots[j];
						lengthM++;
					}
				}
				grandeurMots = newMot.length;
			//heureDebut en minute
			var deb = "";
			if(newMot[1].length == 2){
				deb = newMot[1].charAt(0);
			}else{
				deb = newMot[1].charAt(0)+newMot[1].charAt(1);
			}
			var minDeb = parseInt(deb, 10);
			minDeb = minDeb*60;
			newMot[1] = minDeb;

			//heureFin en Minute
			var fin = "";
			if(newMot[2].length == 2){
				fin = newMot[2].charAt(0);
			}else {
				fin = newMot[2].charAt(0)+newMot[2].charAt(1);
			}
			var minFin = parseInt(fin, 10);
			minFin = minFin*60;
			newMot[2] = minFin;
			
			indisponibilite = indisponibilite + "\t< \""+newMot[0]+"\","+minDeb+","+minFin+">,\n";
			
			
		}else if(mots[0] == "adresse"){
			var newMot = new Array();
			var lengthM = 0;
			for(j=0; j<grandeurMots; j++){
				if(mots[j] == '' || mots[j] == '\t' || mots[j] == 'adresse'){
					delete mots[j];
				}else {
					newMot[lengthM] = mots[j];
					lengthM++;
				}
			}
			grandeurMots = newMot.length;
			var defMot = new Array();
			var lengthDef = 0;
			var place=0;
			var Ville=1;
			var DejaChange = 0;
			var assemblagePlace="";
			var assemblageVille="";
			for(j = 1; j<grandeurMots; j++){
				if(newMot[j] != null || newMot[j] != '' || newMot[j] != ('\t')){
					var temp = newMot[j];
					var lengthmo = temp.length;
					if(temp.charAt(lengthmo-1) == ','){
						if(DejaChange == 0){
							for(l=0; l<temp.length; l++){
								if(temp.charAt(l) != ',' && temp.charAt(l) != " "){
									assemblagePlace = assemblagePlace + temp.charAt(l);
								}else {
									place = 1;
									Ville = 0;
									DejaChange == 1;
								}
							}
						}
					}else
					if(place == 0){
						for(l=0; l<temp.length; l++){
							if(temp.charAt(l) != " " && temp.charAt(l) != '\t'){
								assemblagePlace = assemblagePlace + temp.charAt(l);
							}
						}
						assemblagePlace = assemblagePlace + " ";
					}else
					if(Ville == 0){
						assemblageVille = assemblageVille + newMot[j]+" ";
					}
				}
			}
			lengthDef = 2;
			defMot[0] = newMot[0];
			defMot[1] = assemblagePlace;
			defMot[2] = assemblageVille; 
			grandeurMots = defMot.length;
	
			adresse = adresse + "\t< \""+defMot[0]+"\", \""+defMot[1]+"\", \""+defMot[2]+"\" >,\n";
			

		}else if(mots[0] == "infirmiere"){
			//on met dans tuples ou variable
			var newMot = new Array();
			var lengthM = 0;
			for(j=0; j<grandeurMots; j++){
				if(mots[j] == '' || mots[j] == '\t' || mots[j] == 'infirmiere'){
					delete mots[j];
				}else {
					newMot[lengthM] = mots[j];
					lengthM++;
				}
			}
			grandeurMots = newMot.length;
			//Ajouter dans le tableau de tuple
			infirmiere = infirmiere + "< \""+newMot[0]+"\" >,\n";

			
		}else if(mots[0] == "journee"){
			//on met dans tuplus ou variable
			var newMot = new Array();
			var lengthM = 0;
			for(j = 0; j<grandeurMots; j++){
				if(mots[j] == '' || mots[j] == '\t' || mots[j] == 'journee'){
					delete mots[j];
				}else {
					newMot[lengthM] = mots[j];
					lengthM++;
				}
			}
			grandeurMots = newMot.length;

			//heureDeb en minute
			var deb = "";
			if(newMot[0].length == 2){
				deb = newMot[0].charAt(0);
			} else {
				deb = newMot[0].charAt(0)+newMot[0].charAt(1);
			}
			var minDeb = parseFloat(deb);
			minDeb = minDeb*60;
			newMot[0] = minDeb;
		
			//heureFin en minute
			var fin = "";
			if(newMot[1].length == 2){
				fin = newMot[1].charAt(0);
			}else {
				fin = newMot[1].charAt(0)+newMot[1].charAt(1);
			}
			var minFin = parseFloat(fin);
			minFin = minFin*60;
			newMot[1] = minFin;
			
			journee = journee +"\t<"+ minDeb+","+minFin+">};\n";

			
		}else if(mots[0] == "acte"){
			var newMot = new Array();
			var lengthNewMot = 0;
			for(j=0; j<15; j++){
				if(mots[j] == '' || mots[j] == '\t' || mots[j] == "acte"){
					delete mots[j];
				}else{
					newMot[lengthNewMot] = mots[j];
					lengthNewMot++;
				}
			}
			grandeurMots = newMot.length;
			//Verifier qu'on a bien ce qu'on veut
				var defMot = new Array();
				var lengthDefMot = 0;
				for(j=0; j< grandeurMots; j++){
					if(newMot[j] != null){
					var temp = newMot[j];
					var temp2 = temp.split("\t");
					if(temp2.length > 1){
						for(k=0; k<temp2.length; k++){
							if(temp2[k] == '' || temp2[k] == "acte"){
								delete temp2[j];
							}else {
								defMot[lengthDefMot] = temp2[k];
								lengthDefMot++;
							}
						}
					}else {
						defMot[lengthDefMot] = newMot[j];
						lengthDefMot++;
					}
					}
				}
			grandeurMots = defMot.length;
			
			while(grandeurMots != 2){
				delete defMot[grandeurMots];
				grandeurMots--;
			}
			
			a = a + "\t< \""+defMot[0]+"\","+defMot[1]+">,\n";

		}
	}
	
	soins = soins + "\t <\"cabinet\", \"cab2\",0,1440> };\n";
	indisponibilite = indisponibilite + "};\n";
	adresse = adresse + "};\n";
	a = a + "\t <\"cab2\",0> };\n";
	infirmiere = infirmiere + "};\n";
	e = e + "};\n";
	
	write_in_file(adresse, journee, soins, a,indisponibilite,infirmiere,e);

}

