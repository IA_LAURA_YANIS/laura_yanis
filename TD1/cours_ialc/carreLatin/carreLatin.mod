/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int n = 5;
int count = 0;



  
 
/*** Variables et domaines  ***/
range r = 1..n;

/* Paramétrage du solveur */
execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	cp.param.logVerbosity = "Quiet";
}
dvar int Var [r][r] in r;

include "../shared/displayFirstAndCountSolutions.mod";

/*** Contraintes  ***/
constraints {

	/* pas2 fois le meme nombre dans ligne et colonne */
	forall (i in r ){
		forall(j in r){
			forall(k in r : j<k){
				Var [i][j] != Var [i][k];
				Var [j][i] != Var [k][i];
				}
		  }
}
	
	
}

/*** Post-traitement  ***/
execute {
	count++;
	if ( count==1) {
	writeln (" var   " , Var);}
	
	}
	


