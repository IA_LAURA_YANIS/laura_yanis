/*********************************************
 * Modèle OPL Pour le problème des billes
 *
 * Dans ce modèle, chaque variable doit être déclarée explicitement
 * avec son nom d'origine dans le modèle initial
 *
 *********************************************/
using CP;			/* Utilisation du solveur CP-Solver */


range age = 4..7;
/* Déclarations domaines et variables */
dvar int Anne in age;
dvar int Bernard in age;
dvar int Claudine in age;
dvar int Denis in age;
dvar int Bleu in age;
dvar int Jaune in age;
dvar int Noir in age;
dvar int Rouge in age;
dvar int Parc in age;
dvar int Jardin in age;
dvar int Chambre in age;
dvar int Salon in age;

/* Paramétrage du solveur */

execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
}

/* Contraintes */

constraints {

	/*Denis joue dans le parc et n’a pas 4 ans, contrairement à l’enfant qui a des billes bleues.*/
	Denis == Parc; 
	Denis != 4; 
	Bleu == 4;

	/*La fille de 6 ans a des billes jaunes.*/
	
	Jaune == 6;
	Bernard != 6;
	Denis !=6;
	
	/*L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue dans le jardin mais plus jeune que Anne.*/
	Noir > Jardin;
	Noir < Anne;
	
	/*Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.*/
    	Anne == Chambre; 
	Anne == Salon+1;
	
	/*Contraintes de différence*/

	/*Prenoms*/
	Anne != Bernard; 
	Anne != Claudine; 
	Anne != Denis;
   	Bernard != Claudine; 
	Bernard != Denis; 
	Claudine != Denis;

	/*Couleur*/
   	Bleu != Jaune; 
	Bleu != Noir; 
	Bleu != Rouge;
   	Jaune != Noir; 
	Jaune != Rouge; 
	Noir != Rouge;
	
	/*Lieu*/
   	Parc != Jardin; 
	Parc != Chambre; 
	Parc != Salon;
   	Jardin != Chambre; 
	Jardin != Salon; 
	Chambre != Salon; }

/* Post-traitement (Affichage Solution) */
 


execute {
	writeln("Anne = ", Anne);
	writeln("Bernard = ", Bernard);
	writeln("Claudine = ", Claudine);
	writeln("Denis = ", Denis);
	writeln(" ");
	writeln("Bleu = ", Bleu);
	writeln("Jaune = ", Jaune);
	writeln("Noir = ", Noir);
	writeln("Rouge = ", Rouge);
	writeln(" ");
	writeln("Parc = ", Parc);
	writeln("Jardin = ", Jardin);
	writeln("Chambre = ", Chambre);
	writeln("Salon = ", Salon);
}

