/*********************************************
 * Modèle OPL Pour le problème des billes
 *
 * Utiliser un (ou plusieurs) tableau(x) afin de regrouper les Variables 
 * dans des structures adéquates.
 * Cela doit permettre de simplifier l'énoncé de certaines contraintes 
 * ainsi que l'affichage de la solution.
 *********************************************/
using CP;			/* Utilisation du solveur CP-Solver */

/* Déclarations domaines et Variables */
range ages = 4..7;

{string} prenoms = {"Anne", "Bernard", "Claudine", "Denis"};
{string} couleurs = {"Bleu", "Jaune", "Noir", "Rouge"};
{string} lieux = {"Parc", "Jardin", "Chambre", "Salon"};

{string} nomvars = prenoms union couleurs union lieux;

dvar int Var [nomvars] in ages;  

/* Paramétrage du solveur */
execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
}


/* Contraintes */

constraints {

	/*Var["Denis"] joue dans le Var["Parc"] et n’a pas 4 ans, contrairement à l’enfant qui a des billes Var["Bleu"]es.*/

	Var["Denis"] == Var["Parc"]; 
	Var["Denis"] != 4; 
	Var["Bleu"] == 4;

	/*La fille de 6 ans a des billes Var["Jaune"]s.*/
	
	Var["Jaune"] == 6;
	Var["Bernard"] != 6;
	Var["Denis"] !=6;
	
	/*L’enfant qui joue avec des billes Var["Noir"]es est plus âgé que l’enfant qui joue dans le Var["Jardin"] mais plus jeune que Var["Anne"].*/
	Var["Noir"] > Var["Jardin"];
	Var["Noir"] < Var["Anne"];
	
	/*Var["Anne"], qui joue dans sa Var["Chambre"], a 1 an de plus que l’enfant qui joue dans le Var["Salon"].*/
    	Var["Anne"] == Var["Chambre"]; 
	Var["Anne"] == Var["Salon"]+1;
	
	/*Contraintes de différence*/

	
	/*Prenoms*/
	forall ( i,j in prenoms : i<j){
		Var[i]!= Var[j];
	}
	

	/*Couleur*/	
   	forall ( i,j in couleurs : i<j){
		Var[i]!= Var[j];
	}

	/*Lieu*/
   	forall ( i,j in lieux : i<j){
		Var[i]!= Var[j];
	} 

}


/* Post-traitement (Affichage Solution) */


main {
	var compteur = 0;
	thisOplModel.generate();
	cp.startNewSearch();
	while (cp.next()){
		compteur = compteur+1;
		thisOplModel.postProcess();
}
	compteur;
}
execute {
	writeln (" var " , Var);
}


