/*********************************************
 * Generic main bloc for computing and displaying all solutions
 * as well as the total number of solutions
 *********************************************/
 

main {
	var compteur = 0;
	thisOplModel.generate();
	cp.startNewSearch();
	while (cp.next()){
		compteur = compteur+1;
		thisOplModel.postProcess();
}
	compteur;
}

