/*********************************************
 * Generic main bloc for displaying the first found solution 
 * and displaying the total number of solutions
 *********************************************/
 
main {
	var compteur = 0;
	thisOplModel.generate();
	cp.startNewSearch();
	
	while (cp.next()){
		compteur = compteur+1;
		thisOplModel.postProcess();
}
	compteur;
}
