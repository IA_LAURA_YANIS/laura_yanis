using CP;

// ----- Structures de données pour décrire une instance de problème -----

// Modélisation des informations caractérisant une tâche
tuple Tache{
    string code;		// le code de la tache
    int duree;		    // la duree de la tache
    int puissance;		// la puissance consommee par la tache
}

// Modélisation des contraintes d'ordonnancement à respecter
tuple Ord{
   string avant;		// le code de la tache qui doit se derouler en premier
   string apres;		// le code de la tache qui doit se derouler en second 	
}

//----------------------- Données ---------------------------
{Tache} taches = ...;        // les taches du probleme
{Ord}  cords = ...;       // les contraintes d'ordonnancement entre taches
int	  puissanceMax = ...;   // la puissance maximale de l'usine

//----------------------- Pretraitement ---------------------------
	    
//un pre traitement?
{string} codeTache = {e.code | e in taches};

int duree[codeTache] = [ t.code : t.duree | t in taches];
int dureeMax = sum(i in codeTache) duree[i];
int sommeTotalDuree = sum(i in codeTache) duree[i];
range totalDuree = 0..sommeTotalDuree;



//----------------------- Modèle ---------------------------

// -- variables de décisions --


dvar int debutTache [codeTache] in 0..dureeMax;
dvar int finTache [codeTache] in 0..dureeMax;


// -- variables de commodité --

// -- Critère d'optimisation --
	
minimize 
	max(i in codeTache) (debutTache[i] + duree[i]);
   // Objectif - A completer
subject to {
    // Contraintes - A completer
    forall (i in taches) {

	finTache[i.code] == debutTache[i.code] + i.duree;

}
    forall(c in cords) {
	debutTache[c.apres] >= finTache[c.avant];
}
    forall(j in 0..sommeTotalDuree) {
	(sum ( t in taches ) ((debutTache[t.code]<= j && finTache[t.code]>j)*t.puissance))<=puissanceMax ;
	}
// ce dessus notre calcul sur les puissance pour que 

    


}



//----------------------- Affichage Solution ---------------------------

execute {
    writeln (debutTache);
    writeln (finTache);


    for(var i in totalDuree){
	puissanceUsed = 0;
	for(var t in taches) {
		
		if ( debutTache[t.code] <=i && finTache[t.code]>i) { puissanceUsed = puissanceUsed + t.puissance;}		
	}
		
	
	writeln(puissanceUsed);
       }

}
