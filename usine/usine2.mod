using CP;

// ----- Structures de données pour décrire une instance de problème -----

// Modélisation des informations caractérisant une tâche
tuple Tache{
    string code;		// le code de la tache
    int duree;		    // la duree de la tache
    int puissance;		// la puissance consommee par la tache
}

// Modélisation des contraintes d'ordonnancement à respecter
tuple Ord{
   string avant;		// le code de la tache qui doit se derouler en premier
   string apres;		// le code de la tache qui doit se derouler en second 	
}

//----------------------- Données ---------------------------
{Tache} taches = ...;        // les taches du probleme
{Ord}    cords = ...;       // les contraintes d'ordonnancement entre taches
int	  puissanceMax = ...;   // la puissance maximale de l'usine

//----------------------- Pretraitement ---------------------------
	    
// A compléter si nécessaire
{string} codeTache = {e.code | e in taches};
int sommeduree = ftoi(sum(i in taches) i.duree);
int dureeMax = ftoi(max(i in taches) i.duree);
int dureeMin = ftoi(min(i in taches) i.duree); 
range touteduree = 0..sommeduree;
//----------------------- Modèle ---------------------------

// -- variables de décisions --
dvar interval intTache[codeTache] in 0..sommeduree size dureeMin..dureeMax ;

// -- variables de commodité --

// -- Critère d'optimisation --



minimize 
   max ( i in codeTache) endOf(intTache[i]);
subject to {

    (sum(tt in taches) pulse(intTache[tt.code],tt.puissance))<= puissanceMax;
    forall(i in taches) {
	sizeOf(intTache[i.code]) == i.duree;
	endOf(intTache[i.code]) == startOf(intTache[i.code]) + i.duree;

	

}
    // Contraintes - A completer
    forall(c in cords) {
	
	startBeforeStart(intTache[c.avant],intTache[c.apres],sizeOf(intTache[c.avant]));

	//endOf(intTache[c.avant])>= startOf(intTache[c.apres]);
}
}	


//----------------------- Affichage Solution ---------------------------

execute {
    for(var i in taches) {
	writeln("");
    	write( Opl.startOf(intTache[i.code]) );
	write(" - ");
	writeln( Opl.endOf(intTache[i.code]))};

   
     for(var i in touteduree){
	puissanceUsed = 0;
	for(var j in taches){
		if(Opl.startOf(intTache[j.code]) <= i && i< Opl.endOf(intTache[j.code])){
		puissanceUsed = j.puissance + puissanceUsed;
		}	
	}
	writeln(" 0    ",puissanceUsed);
       }
}
