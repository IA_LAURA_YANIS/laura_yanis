/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

 
/*** Variables et domaines  ***/
range r = 0..12;
{string} cardinalites = {"Cne", "Cse", "Cno", "Cso", "Nord", "Sud", "Est", "Ouest"}; 
dvar int a[cardinalites] in r;


/* Paramétrage du solveur */
execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	cp.param.logVerbosity = "Quiet";
}


/*** Contraintes  ***/

constraints {
	(a["Cne"]+a["Nord"]+a["Cno"])>=5;
	(a["Cne"]+a["Est"]+a["Cse"])>=5;
	(a["Cse"]+a["Sud"]+a["Cso"])>=5;
	(a["Cso"]+a["Ouest"]+a["Cno"])>=5;

	a["Cne"]+a["Nord"]+a["Cno"]+a["Est"]+a["Cse"]+a["Sud"]+a["Cso"]+a["Ouest"] == 12;
}


/*** Post-traitement  ***/
execute { writeln("       ", a); }
	



