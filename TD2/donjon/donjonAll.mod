/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

 
/*** Variables et domaines  ***/
range r = 0..12;
{string} cardinalites = {"Cne", "Cse", "Cno", "Cso", "Nord", "Sud", "Est", "Ouest"}; 
dvar int a[cardinalites] in r;


/* Paramétrage du solveur */
execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	cp.param.logVerbosity = "Quiet";
}

main {
	var compteur = 0;
	thisOplModel.generate();
	cp.startNewSearch();
	while (cp.next()){
		compteur = compteur+1;
		thisOplModel.postProcess();
}
	compteur;
}

/*** Contraintes  ***/

constraints {
	(a["Cne"]+a["Nord"]+a["Cno"]) >= 5;
	(a["Cne"]+a["Est"]+a["Cse"]) >= 5;
	(a["Cse"]+a["Sud"]+a["Cso"]) >= 5;
	(a["Cso"]+a["Ouest"]+a["Cno"]) >= 5;

	a["Cne"]+a["Nord"]+a["Cno"]+a["Est"]+a["Cse"]+a["Sud"]+a["Cso"]+a["Ouest"] == 12;
}


/*** Post-traitement  ***/
execute { writeln("       ", a); }
	












/** symétrie        **\



Si j'enlève une solution avec symétrie puis-je déduire par symétrie celle que j'ai éléminé.

NO <= NE

empéche de produire les solutions 2 0 3     pas celle la 3 0 2
				  1   1                  1   1
				  3 0 2                  2 0 3

On pourra jsute supprimer quelques solutions pas toutes

NO <= SO
NO <= SE
SE <= NE
























