/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

 
/*** Variables et domaines  ***/
int n = 4;
range r = 0..n*n;
range lc = 0..n;


dvar int a[lc, lc] in r;

int old=0;
int diagG =0;
int diagD =0;
int tamponL =0;
int tamponC =0;

/* Paramétrage du solveur */
execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	cp.param.logVerbosity = "Quiet";
}


/*** Contraintes  ***/

/*** FAUX CAR PAS POSSIBLE DE FAIRE DES CALCULS DANS LES CONTRAINTES ***/
/*** Remplacer par des sum peut-etre ??? ***/
/** faire des sommes de lignes egale***/
/*** on sait qu'elle est cette valeur d'égalité ***/
/*** somme de toutes les lignes et somme * n  qui est egale somme de 1 à n*n
     n*n(n*n+1)/2 = n*s
	s= n*(n*n+1)/2   ***/
	
constraints {
		
	forall ( i in lc){
		tamponL = 0;
		tamponC = 0;
		
		forall ( j in lc) {
			tamponL = tamponL + a[i][j];
			tamponC = tamponC + a[j][i];
			
			if (i == j){
				diagG = diagG + a[i][j];
						
					};
			if ( i+j == n){
				diagD = diagD + a[i][j];}
}		
		if (i==0){
			old=tamponL;
			tamponL == tamponC;}
		else {
			tamponL == old;
			tamponC == old;}
	
		
			
	}
	diagG == old;
	diagD == old; 
		
	

}


/*** Post-traitement  ***/
execute { writeln("       ", a); }
	



