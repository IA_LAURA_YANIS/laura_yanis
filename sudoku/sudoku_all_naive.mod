/*********************************************
 * Modèle pour le problème du sudoku nxn
 * Modèle naif
 *********************************************/
using CP;

// --- Parametrage Solveur ---



// --- Données ---
int n = ...	;		/* square order*/
range rsud = 1..n;
int instance[rsud][rsud] = ...;



// --- Pretraitement ---

int tailleCase = ftoi(sqrt(n));

/* recuperer la racine de n pour trouver la taille de notre carre*/




execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	/*cp.param.logVerbosity = "Quiet";*/
	cp.param.allDiffInferenceLevel = "Low"; /* gracve au extended on a gagné du temps d'execution on est passé de 0.01s à 0.00s */
						/* Sur le Low on est a 0.02s de temps d'execution */
						/* Sur le Medium on est a 0.01s de temps d'execution */
}


/* pour le quiet */
// --- Modèle ---
dvar int sudoku[rsud][rsud] in rsud; /*c'est mon sudoku résolu */
				     /* c'est un matrice n*n */
	





// --- Main (pour trouver toutes les solution + statistiques) ---

main {
	var compteur = 0;
	thisOplModel.generate();
	cp.startNewSearch();
	
	while (cp.next()){
		compteur = compteur+1;
		thisOplModel.postProcess();
}
	compteur;
}

/* compter et avoir toutes les solutions */


constraints{
	forall(i in rsud) {
		forall(k in rsud) {
			if(instance[i][k] != 0 ){
				sudoku[i][k] == instance[i][k];
}
}
}  

/* on va verifier que notre sudoku reprenne bien nos valeurs de base */


forall (i in rsud) {
	allDifferent ( all (j in rsud) sudoku[i][j]);
	allDifferent ( all (j in rsud) sudoku[j][i]);
		   }

/* ici on verifier que nos lignes et nos colonnes sont différrentes */

forall(i in 0..tailleCase-1){
	forall(j in 0..tailleCase-1){
		allDifferent(all(colonne,ligne in 1..tailleCase: (1<=tailleCase*i+colonne<=n) && (1<=tailleCase*i+ligne<=n) ) sudoku[tailleCase*i+colonne][tailleCase*i+ligne] ) ; 
}
}

	

}
// --- PostTraitement --- (affichage solution)

execute { write("       ");
	  writeln(sudoku);
	  writeln("                      ");	}




