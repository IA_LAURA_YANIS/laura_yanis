/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int n = ...;



  
 
/*** Variables et domaines  ***/
range r = 1..n;

dvar int a[r] in r;


/* Paramétrage du solveur */
execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	cp.param.logVerbosity = "Quiet";
}


/*** Contraintes  ***/

constraints {
	forall (i, j in r : i < j) {
		a[i] != a[j];
		abs(a[j]-a[i]) != abs(j-i); 
	}	
}


/*** Post-traitement  ***/
execute { writeln("       ", a); }
	



