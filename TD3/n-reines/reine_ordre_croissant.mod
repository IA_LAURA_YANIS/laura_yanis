/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int n = ...;



  
 
/*** Variables et domaines  ***/
range r = 1..n;
int b[i in r] = i;
dvar int a[r] in r;



/* Paramétrage du solveur */
execute {
	
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	cp.param.logVerbosity = "Quiet";
	var f = cp.factory;
	cp.setSearchPhases(
			f.searchPhase(a,
				f.selectSmallest(f.explicitVarEval(a,b)),
				f.selectSmallest(f.value())
					)
			   );
}


/*** Contraintes  ***/

constraints {
	forall (i, j in r : i < j) {
		a[i] != a[j];
		abs(a[j]-a[i]) != abs(j-i); 
	}	
}


/*** Post-traitement  ***/
execute { writeln("       ", a); 
	  writeln("        ", cp.info.solveTime);
	  writeln(" fail   ",cp.info.numberofFails);
	  writeln(" choices ", cp.info.numberOfChoicePoints);
	  }
	



