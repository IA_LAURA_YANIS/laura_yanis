/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/

range monnaie = 0..99;
  
 
/*** Variables et domaines  ***/

{int} val_piece = {1,2,5,10,20,50};

dvar int toutsomme [s in monnaie][v in val_piece] in 0..(s div v);

dvar int portefeuille[v in val_piece] in 0..(99 div v) ;


/* Paramétrage du solveur */

minimize
	sum(i in val_piece) portefeuille[i];
	

/*** Contraintes  ***/

subject to {
	forall (k in monnaie) {
	
		
		sum(i in val_piece) toutsomme[k][i]*i == k;    

/** il faut avoir la valeur de monnaie **/
		
		forall (i in val_piece) {
			toutsomme[k][i] <= portefeuille[i]; 

 /** il faut que les pieces utilisées soient inférieur aux pièces dans le porte monnaie   **/

		}
}
}

execute {
	
	for (var m in monnaie) {
		write("pour somme ",m, "||");
		for(var v in val_piece) {
			write(" ",toutsomme[m][v]," ");}
		writeln(" ");}

	writeln("notre porefeuille est de " , portefeuille);
}
/*** Post-traitement  ***/




