/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int n = 8; /* nombre de n cases **/



  
 
/*** Variables et domaines  ***/
range r = 1..n;   /* nbr de reines */

dvar int plateau[r][r] in 0..1; /* la reine est placé ou pas sur notre plateau */


/* Paramétrage du solveur */
execute {
	cp.param.searchType = "DepthFirst";
	cp.param.workers=1;
	cp.param.logVerbosity = "Quiet";
}

minimize
	sum(i,j in r) plateau[i][j] ;
/*** Contraintes  ***/

subject to {
	forall (i, j in r ) {

		(sum (k in r) plateau[i][k] +	
		sum(k in r) plateau[k][j] + 
		
		sum(k in 1..n :  maxl( i+k,j+k) <=n ) plateau[i+k][j+k]  +
		sum(k in 1..n :  (1<= i-k <= n) && (1<= j-k <= n)) plateau[i-k][j-k] +
		
		sum(k in 1..n : (1<= i+k <=n) && (1<= j-k <=n)) plateau[i+k][j-k] +
		sum(k in 1..n :  (1<= i-k <= n) && (1<= j+k<=n)) plateau[i-k][j+k]  )	>= 1 ;  /* on verifie la ligne puis la colonne puis la diagonale */
		
		 
		
	}	
}


/*** Post-traitement  ***/
execute { writeln("       ", plateau); }
	



