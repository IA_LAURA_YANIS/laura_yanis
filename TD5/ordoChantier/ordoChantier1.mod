/***********************************************
*  NOM 1 - NOM 2          <-- Remplacer ici
************************************************/
using CP;
/*********** Données ***********/
// Completer ICI

tuple Tache  {
		 string codeTache;
		 string nomTache;
		 int dureeTache;
		 {string} dependanceTache;
};
		  
		  
 		
{Tache} TachesChantier = ...;









/*********** Prétraitement ***********/

// Completer ICI - Extraction des informations utiles à partir des données brutes

{string} codesChantier = {e.codeTache | e in TachesChantier};  
	/* j'importe mes codeChantier */


int duree[codesChantier] = [ t.codeTache : t.dureeTache | t in TachesChantier]; 
	/* pour chaque codeChantier je veux ça durée*/

{string} dependance[codesChantier] = [t.codeTache : t.dependanceTache | t in TachesChantier];

int minimumDuree = min(i in codesChantier) duree[i]; 
int n = sum(i in codesChantier) duree[i] - minimumDuree - minimumDuree;


/*********** Modèle ***********/


// Completer ICI - Variables de décisions

dvar int debutTache[codesChantier] in 0..n; /* le debut de ma tache : quand elle commence?*/

// Completer ICI - Objectif et Contraintes




minimize
	max(i in codesChantier) (debutTache[i] + duree[i]);  
	
	/** le max de la derniere tache + ca durée
	* indique quand le chantier finis */

subject to {
	forall ( i in codesChantier) {

		debutTache[i] >= max (e in dependance[i])(duree[e] + debutTache[e]);

}
	 debutTache["I"] > debutTache["H"]+10; /* question 2 */ 
	debutTache["K"] != debutTache["M"];  /*question 3 */ 

} 

/*********** Post-traitement ***********/

// Completer ICI - Affichage de la solution

execute {
	
	writeln(codesChantier);
	write(debutTache);

	
		}
